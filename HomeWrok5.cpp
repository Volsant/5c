﻿#include <iostream>


class Vector
{
private:
	int x = 0; 
	int y = 0;
	int z = 0;
public:
	
	Vector () : x(3), y(3), z(3)  // Задаем параметры для вектора (ось x,y,z)
	{}

	void Show()
	{
		std::cout << "x = " << x << "; y = " << y << "; z = " << z;   // Отбражение параметров вектора
	}
		
	double GetVector()
	{
		double Lenght = x * x + y * y + z * z;

		return sqrt(Lenght);  // Возвращаем длину вектора
	}
	
};


int main()
{
	Vector vtemp, ltemp;

	vtemp.Show();
	std::cout << "\n";   // Вызываем функцию для вывода в консоль данных вектора

	ltemp.GetVector();  // Вызываем функцию для подсчета длины вектора

	std::cout << "Vector Lenght = " << ltemp.GetVector();  // Выводим длинуц вектора в консоль
}


